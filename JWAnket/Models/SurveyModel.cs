﻿using System;
using System.Collections.Generic;

namespace JWAnket.Models
{
    public class SurveyModel
    {
        public SurveyModel()
        {
            WhiskeyBrandList = new List<CheckBoxList>();
            ReasonList = new List<CheckBoxList>();
            OtherWhiskeyBrandList = new List<CheckBoxList>();
            ChangedWhiskeyBrandList = new List<CheckBoxList>();
            CreateDate = DateTime.Now;
        }

        public List<CheckBoxList> WhiskeyBrandList { get; set; }
        public string OtherWhiskeyBrandText { get; set; }

        public List<CheckBoxList> ReasonList { get; set; }
        public string OtherReasonText { get; set; }


        public List<CheckBoxList> OtherWhiskeyBrandList { get; set; }
        public string YetAnotherWhiskeyBrandText { get; set; }


        public List<CheckBoxList> ChangedWhiskeyBrandList { get; set; }
        public string ChangedWhiskeyBrandText { get; set; }

        public string Comments { get; set; }
        public DateTime CreateDate { get; set; }

        public string NameSurname { get; set; }
        public string Phone { get; set; }
        public string EMail { get; set; }
    }

    public class CheckBoxList
    {
        public string Name { get; set; }
        public bool IsSelected { get; set; }
        public int Id { get; set; }
    }
}