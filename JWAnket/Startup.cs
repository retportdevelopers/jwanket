﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JWAnket.Startup))]
namespace JWAnket
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
