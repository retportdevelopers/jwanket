﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using JWAnket.Models;

namespace JWAnket.Controllers
{
    public class SurveyController : Controller
    {
        public ActionResult BeginSurvey()
        {
            return View();
        }

        //Mentoring Öncesi
        public ActionResult BeforeSurvey()
        {
            return View();
        }

        public ActionResult FirstPage()
        {
            SurveyModel currentModel = new SurveyModel
            {
                WhiskeyBrandList = new List<CheckBoxList>
                {
                    new CheckBoxList {Id = 1, Name = "JW Red Label"},
                    new CheckBoxList {Id = 2, Name = "JW Black Label"},
                    new CheckBoxList {Id = 3, Name = "JW Double Black Label"},
                    new CheckBoxList {Id = 4, Name = "Jack Daniels"},
                    new CheckBoxList {Id = 5, Name = "Chivas 12"},
                    new CheckBoxList {Id = 6, Name = "Chivas 18"},
                    new CheckBoxList {Id = 7, Name = "Dimple Golden Selection"},
                    new CheckBoxList {Id = 8, Name = "JW Gold Label"},
                    new CheckBoxList {Id = 9, Name = "JW Platinum Label"},
                    new CheckBoxList {Id = 10, Name = "JW Blue Label"},
                    new CheckBoxList {Id = 11, Name = "Cardhu"},
                    new CheckBoxList {Id = 12, Name = "Glenkinchie"},
                    new CheckBoxList {Id = 13, Name = "Talisker"},
                    new CheckBoxList {Id = 14, Name = "Lagavulin"},
                    new CheckBoxList {Id = 15, Name = "Singleton 12"},
                    new CheckBoxList {Id = 16, Name = "Singleton 15"},
                    new CheckBoxList {Id = 17, Name = "Jim Beam"},
                    new CheckBoxList {Id = 18, Name = "Ballantines"},
                    new CheckBoxList {Id = 19, Name = "Glenlivet "},
                    new CheckBoxList {Id = 20, Name = "Mac Allen"},
                    new CheckBoxList {Id = 21, Name = "Jameson"},
                    new CheckBoxList {Id = 22, Name = "Glenfiddich"},
                    new CheckBoxList {Id = 23, Name = "Bush Mils"},
                    new CheckBoxList {Id = 24, Name = "Viski İçmiyorum"}
                }
            };

            Session["SurveyModel"] = currentModel;

            return View(currentModel);
        }

        [HttpPost]
        public ActionResult SecondPage(SurveyModel surveyModel)
        {
            SurveyModel currentModel = (SurveyModel)Session["SurveyModel"];

            currentModel.ReasonList = new List<CheckBoxList>
            {
                new CheckBoxList {Id = 1, Name = "Viskinin Türü (Scotch, Tennessee, Bourbon vb.)"},
                new CheckBoxList {Id = 2, Name = "Tat"},
                new CheckBoxList {Id = 3, Name = "Alışkanlık"},
                new CheckBoxList {Id = 4, Name = "Fiyat"},
                new CheckBoxList {Id = 5, Name = "Ambalaj - Şişe"},
                new CheckBoxList {Id = 6, Name = "Arkadaşlarımın İçtiği Marka"}
            };

            currentModel.WhiskeyBrandList = surveyModel.WhiskeyBrandList;
            currentModel.OtherWhiskeyBrandText = surveyModel.OtherWhiskeyBrandText;

            Session["SurveyModel"] = currentModel;

            return View(currentModel);
        }

        [HttpPost]
        public ActionResult ThirdPage(SurveyModel surveyModel)
        {
            SurveyModel currentModel = (SurveyModel)Session["SurveyModel"];

            currentModel.OtherWhiskeyBrandList = new List<CheckBoxList>
            {
                new CheckBoxList {Id = 1, Name = "JW Red Label"},
                new CheckBoxList {Id = 2, Name = "JW Black Label"},
                new CheckBoxList {Id = 3, Name = "JW Double Black Label"},
                new CheckBoxList {Id = 4, Name = "Jack Daniels"},
                new CheckBoxList {Id = 5, Name = "Chivas 12"},
                new CheckBoxList {Id = 6, Name = "Chivas 18"},
                new CheckBoxList {Id = 7, Name = "Dimple Golden Selection"},
                new CheckBoxList {Id = 8, Name = "JW Gold Label"},
                new CheckBoxList {Id = 9, Name = "JW Platinum Label"},
                new CheckBoxList {Id = 10, Name = "JW Blue Label"},
                new CheckBoxList {Id = 11, Name = "Cardhu"},
                new CheckBoxList {Id = 12, Name = "Glenkinchie"},
                new CheckBoxList {Id = 13, Name = "Talisker"},
                new CheckBoxList {Id = 14, Name = "Lagavulin"},
                new CheckBoxList {Id = 15, Name = "Singleton 12"},
                new CheckBoxList {Id = 16, Name = "Singleton 15"},
                new CheckBoxList {Id = 17, Name = "Jim Beam"},
                new CheckBoxList {Id = 18, Name = "Ballantines"},
                new CheckBoxList {Id = 19, Name = "Glenlivet "},
                new CheckBoxList {Id = 20, Name = "Mac Allen"},
                new CheckBoxList {Id = 21, Name = "Jameson"},
                new CheckBoxList {Id = 22, Name = "Glenfiddich"},
                new CheckBoxList {Id = 23, Name = "Bush Mils"},
                new CheckBoxList {Id = 24, Name = "Viski İçmiyorum"}
            };

            currentModel.OtherReasonText = surveyModel.OtherReasonText;
            currentModel.ReasonList = surveyModel.ReasonList;

            Session["SurveyModel"] = currentModel;

            return View(currentModel);
        }

        //Mentoring Sonrası
        [HttpPost]
        public ActionResult AfterSurvey(SurveyModel surveyModel)
        {
            SurveyModel currentModel = (SurveyModel)Session["SurveyModel"];

            currentModel.OtherWhiskeyBrandList = surveyModel.OtherWhiskeyBrandList;
            currentModel.YetAnotherWhiskeyBrandText = surveyModel.YetAnotherWhiskeyBrandText;

            Session["SurveyModel"] = currentModel;

            return View(currentModel);
        }

        [HttpPost]
        public ActionResult FourthPage(SurveyModel surveyModel)
        {
            SurveyModel currentModel = (SurveyModel)Session["SurveyModel"];

            currentModel.ChangedWhiskeyBrandList = new List<CheckBoxList>
            {
                new CheckBoxList {Id = 1, Name = "JW Red Label"},
                new CheckBoxList {Id = 2, Name = "JW Black Label"},
                new CheckBoxList {Id = 3, Name = "JW Double Black Label"},
                new CheckBoxList {Id = 4, Name = "Jack Daniels"},
                new CheckBoxList {Id = 5, Name = "Chivas 12"},
                new CheckBoxList {Id = 6, Name = "Chivas 18"},
                new CheckBoxList {Id = 7, Name = "Dimple Golden Selection"},
                new CheckBoxList {Id = 8, Name = "JW Gold Label"},
                new CheckBoxList {Id = 9, Name = "JW Platinum Label"},
                new CheckBoxList {Id = 10, Name = "JW Blue Label"},
                new CheckBoxList {Id = 11, Name = "Cardhu"},
                new CheckBoxList {Id = 12, Name = "Glenkinchie"},
                new CheckBoxList {Id = 13, Name = "Talisker"},
                new CheckBoxList {Id = 14, Name = "Lagavulin"},
                new CheckBoxList {Id = 15, Name = "Singleton 12"},
                new CheckBoxList {Id = 16, Name = "Singleton 15"},
                new CheckBoxList {Id = 17, Name = "Jim Beam"},
                new CheckBoxList {Id = 18, Name = "Ballantines"},
                new CheckBoxList {Id = 19, Name = "Glenlivet "},
                new CheckBoxList {Id = 20, Name = "Mac Allen"},
                new CheckBoxList {Id = 21, Name = "Jameson"},
                new CheckBoxList {Id = 22, Name = "Glenfiddich"},
                new CheckBoxList {Id = 23, Name = "Bush Mils"},
                new CheckBoxList {Id = 24, Name = "Viski İçmiyorum"}
            };

            Session["SurveyModel"] = currentModel;

            return View(currentModel);
        }

        [HttpPost]
        public ActionResult FifthPage(SurveyModel surveyModel)
        {
            SurveyModel currentModel = (SurveyModel)Session["SurveyModel"];

            currentModel.ChangedWhiskeyBrandList = surveyModel.ChangedWhiskeyBrandList;
            currentModel.ChangedWhiskeyBrandText = surveyModel.ChangedWhiskeyBrandText;

            Session["SurveyModel"] = currentModel;

            return View(currentModel);
        }

        [HttpPost]
        public ActionResult SixthPage(SurveyModel surveyModel)
        {
            SurveyModel currentModel = (SurveyModel)Session["SurveyModel"];

            currentModel.NameSurname = surveyModel.NameSurname;
            currentModel.Phone = surveyModel.Phone;
            currentModel.EMail = surveyModel.EMail;

            Session["SurveyModel"] = currentModel;

            return View(currentModel);
        }

        [HttpPost]
        public ActionResult FinalPage(SurveyModel surveyModel)
        {
            SurveyModel currentModel = (SurveyModel)Session["SurveyModel"];

            currentModel.Comments = surveyModel.Comments;

            PersistModel(currentModel);

            return View();
        }

        private void PersistModel(SurveyModel currentModel)
        {
            string databasePath = Server.MapPath("~/Content/survey.sqlite");
            SQLiteConnection conn = new SQLiteConnection(@"Data Source=" + databasePath + ";Version=3;");

            try
            {
                SQLiteCommand cmd = new SQLiteCommand();
                cmd.Connection = conn;
                conn.Open();

                cmd.CommandText =
                    "INSERT INTO JwSurvey(CreateDate, Comments) VALUES(@CreateDate, @Comments); select last_insert_rowid();";
                cmd.Parameters.AddWithValue("CreateDate", currentModel.CreateDate);
                cmd.Parameters.AddWithValue("Comments", currentModel.Comments);

                int surveyId = int.Parse(cmd.ExecuteScalar().ToString());

                foreach (var item in currentModel.WhiskeyBrandList.Where(x => x.IsSelected == true))
                {
                    string query =
                        "INSERT INTO QuestionOne(SurveyId, OptionName) VALUES(@SurveyId,  @OptionName); select last_insert_rowid();";

                    SQLiteCommand cmdQuestion = new SQLiteCommand(query, conn);

                    cmdQuestion.Parameters.AddWithValue("SurveyId", surveyId);
                    cmdQuestion.Parameters.AddWithValue("OptionName", item.Name);
                    cmdQuestion.ExecuteNonQuery();
                }

                if (!string.IsNullOrEmpty(currentModel.OtherWhiskeyBrandText))
                {
                    string query =
                        "INSERT INTO QuestionOne(SurveyId, OptionName) VALUES(@SurveyId,  @OptionName); select last_insert_rowid();";

                    SQLiteCommand cmdQuestion = new SQLiteCommand(query, conn);

                    cmdQuestion.Parameters.AddWithValue("SurveyId", surveyId);
                    cmdQuestion.Parameters.AddWithValue("OptionName", currentModel.OtherWhiskeyBrandText);
                    cmdQuestion.ExecuteNonQuery();
                }

                foreach (var item in currentModel.ReasonList.Where(x => x.IsSelected == true))
                {
                    string query =
                        "INSERT INTO QuestionTwo(SurveyId, OptionName) VALUES(@SurveyId,  @OptionName); select last_insert_rowid();";

                    SQLiteCommand cmdQuestion = new SQLiteCommand(query, conn);

                    cmdQuestion.Parameters.AddWithValue("SurveyId", surveyId);
                    cmdQuestion.Parameters.AddWithValue("OptionName", item.Name);
                    cmdQuestion.ExecuteNonQuery();
                }

                if (!string.IsNullOrEmpty(currentModel.OtherReasonText))
                {
                    string query =
                        "INSERT INTO QuestionTwo(SurveyId, OptionName) VALUES(@SurveyId,  @OptionName); select last_insert_rowid();";

                    SQLiteCommand cmdQuestion = new SQLiteCommand(query, conn);

                    cmdQuestion.Parameters.AddWithValue("SurveyId", surveyId);
                    cmdQuestion.Parameters.AddWithValue("OptionName", currentModel.OtherReasonText);
                    cmdQuestion.ExecuteNonQuery();
                }

                foreach (var item in currentModel.OtherWhiskeyBrandList.Where(x => x.IsSelected == true))
                {
                    string query =
                        "INSERT INTO QuestionThree(SurveyId, OptionName) VALUES(@SurveyId,  @OptionName); select last_insert_rowid();";

                    SQLiteCommand cmdQuestion = new SQLiteCommand(query, conn);

                    cmdQuestion.Parameters.AddWithValue("SurveyId", surveyId);
                    cmdQuestion.Parameters.AddWithValue("OptionName", item.Name);
                    cmdQuestion.ExecuteNonQuery();
                }

                if (!string.IsNullOrEmpty(currentModel.YetAnotherWhiskeyBrandText))
                {
                    string query =
                        "INSERT INTO QuestionThree(SurveyId, OptionName) VALUES(@SurveyId,  @OptionName); select last_insert_rowid();";

                    SQLiteCommand cmdQuestion = new SQLiteCommand(query, conn);

                    cmdQuestion.Parameters.AddWithValue("SurveyId", surveyId);
                    cmdQuestion.Parameters.AddWithValue("OptionName", currentModel.YetAnotherWhiskeyBrandText);
                    cmdQuestion.ExecuteNonQuery();
                }

                foreach (var item in currentModel.ChangedWhiskeyBrandList.Where(x => x.IsSelected == true))
                {
                    string query =
                        "INSERT INTO QuestionFour(SurveyId, OptionName) VALUES(@SurveyId,  @OptionName); select last_insert_rowid();";

                    SQLiteCommand cmdQuestion = new SQLiteCommand(query, conn);

                    cmdQuestion.Parameters.AddWithValue("SurveyId", surveyId);
                    cmdQuestion.Parameters.AddWithValue("OptionName", item.Name);
                    cmdQuestion.ExecuteNonQuery();
                }

                if (!string.IsNullOrEmpty(currentModel.ChangedWhiskeyBrandText))
                {
                    string query =
                        "INSERT INTO QuestionFour(SurveyId, OptionName) VALUES(@SurveyId,  @OptionName); select last_insert_rowid();";

                    SQLiteCommand cmdQuestion = new SQLiteCommand(query, conn);

                    cmdQuestion.Parameters.AddWithValue("SurveyId", surveyId);
                    cmdQuestion.Parameters.AddWithValue("OptionName", currentModel.ChangedWhiskeyBrandText);
                    cmdQuestion.ExecuteNonQuery();
                }

                if (!string.IsNullOrEmpty(currentModel.NameSurname))
                {
                    string query =
                        "INSERT INTO Mentoring(SurveyId, Name) VALUES(@SurveyId,  @Name); select last_insert_rowid();";

                    SQLiteCommand cmdQuestion = new SQLiteCommand(query, conn);

                    cmdQuestion.Parameters.AddWithValue("SurveyId", surveyId);
                    cmdQuestion.Parameters.AddWithValue("Name", currentModel.NameSurname);
                    cmdQuestion.ExecuteNonQuery();
                }

                if (!string.IsNullOrEmpty(currentModel.Phone))
                {
                    string query =
                        "INSERT INTO Mentoring(SurveyId, Phone) VALUES(@SurveyId,  @Phone); select last_insert_rowid();";

                    SQLiteCommand cmdQuestion = new SQLiteCommand(query, conn);

                    cmdQuestion.Parameters.AddWithValue("SurveyId", surveyId);
                    cmdQuestion.Parameters.AddWithValue("Phone", currentModel.Phone);
                    cmdQuestion.ExecuteNonQuery();
                }

                if (!string.IsNullOrEmpty(currentModel.EMail))
                {
                    string query =
                        "INSERT INTO Mentoring(SurveyId, Mail) VALUES(@SurveyId,  @Mail); select last_insert_rowid();";

                    SQLiteCommand cmdQuestion = new SQLiteCommand(query, conn);

                    cmdQuestion.Parameters.AddWithValue("SurveyId", surveyId);
                    cmdQuestion.Parameters.AddWithValue("Mail", currentModel.EMail);
                    cmdQuestion.ExecuteNonQuery();
                }
            }
            catch (SQLiteException ex)
            {
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
