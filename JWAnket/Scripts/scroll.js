﻿var scrolled = 0;
$(document).ready(function () {
    $("#downArrow").on("click", function () {
        scrolled = scrolled + 200;
        $(".cover").stop().animate({
            scrollTop: scrolled
        });
    });

    $("#upArrow").on("click", function () {
        scrolled = scrolled - 200;
        $(".cover").stop().animate({
            scrollTop: scrolled
        });
    });

    $(".clearValue").on("click", function () {
        scrolled = 0;
    });
});