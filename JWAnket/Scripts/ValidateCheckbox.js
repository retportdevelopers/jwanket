﻿function submitWith() {
    var checkedCount = $("input:checked").length;
    var valid = checkedCount > 0;
    if (!valid) {
        $('#validationMessage').html('Lütfen Seçim Yapınız');
    }

    return valid;
}